
 Ztensity's NerdScurvy's Toybox Patches - Version 1.0.0
 Blocks XML Edit For Cleaner Creative Menu
 Build 19.3 Compatible
 Notes Log 1.0.0
 
 
 ================================
 
 
 
 NOTES
 ==============
 
 I noticed that there are 236 individual toys that are literally replicas/copies of the base 30 variations of 10 unique toys. While I'm unsure of what the point of all 236 copies are, I hid them from the creative menu so that things are cleaner when navigating these menu's.
 
 
 ================================