 
 Ztensity's War of the Walkers Patches - Version 1.0.0
 Localization Edits, UI element Tweaks & More
 Build 19.3 Compatible
 Bug Report 1.0.0
 
 
 ================================
 
 
 
 Despite everything I have done to clean up War of the Walkers and make it more smooth, immersive and seamless then ever, I did notice a few bugs and errors that I was unable to fix.
 
 
 YELLOW LINE WRN ERRORS
 ==============
 
 The first error is associated with "creating new hudElements, atlas logo and menuElements." I've never seen this with other overhauls and with SMX UI with a standard set of modlets (30+). See the error below:
 
	2021-02-08T01:08:47 5.154 WRN [MODS] Creating new atlas hudElements for mod War of the Walkers Mod
	2021-02-08T01:08:47 5.175 INF Pack 7702 us
	2021-02-08T01:08:48 6.163 INF Pack 420325 us
	2021-02-08T01:08:48 6.165 WRN [MODS] Creating new atlas logo for mod War of the Walkers Mod
	2021-02-08T01:08:48 6.206 INF Pack 14592 us
	2021-02-08T01:08:48 6.207 WRN [MODS] Creating new atlas menuElements for mod War of the Walkers Mod
 
 I believe that this could potentially get resolved, but no one provided any information on it while I was looking into it.
 
 
 The second error is associated with scripts missing, causing the below error to repeat for a total of 9 times, which could be causing longer loading times when trying to boot up a world.
 
	The referenced script (Unknown) on this Behaviour is missing!
	
	(Filename: C:\buildslave\unity\build\Runtime/Scripting/ManagedReference/SerializableManagedRef.cpp Line: 199)
	
	The referenced script on this Behaviour (Game Object '<null>') is missing!
	
	(Filename: C:\buildslave\unity\build\Runtime/Mono/ManagedMonoBehaviourRef.cpp Line: 334)
 
 Guppy stated in his Discord, "Adreden wrote me a script to find the attached scripts, and it seems to work because if I attach a script for testing it, it'll find it, but I still have 20 plus number twos on my mod." I think that error means an attached scripts as well as something else.
 
 He and assorment of others believe yellow WRN line errors is fine. However, I feel like these errors could be causing long loading times for booting up a game with War of the Walkers and associated modlets included with the overhaul.
 
 Xyth added, "This error is likely a script left on a Unity object."
 
 
 
 RED LINE ERROR (Fixed With DMT / DLLs / Harmony / C# - Check Notes)
 ==============
 
 The third and last error is a red line error that seems to be associated with assets (Unity3d files) loading and while it doesn't prevent players from playing the mod, I'm convinced it could have a negative effect on the War of the Walkers game.
 
	UnloadAsset can only be used on assets;
	 
	(Filename: C:\buildslave\unity\build\Runtime/Scripting/Scripting.cpp Line: 439)
 
 This error seems common as a couple people reported having it in their overhauls and/or bigger modlets as well, however I have never seen this error on overhauls like War3zuk AIO, Ravenhearst and Darkness Falls.
 
 According to Xyth, "The third error is a known issue that Sphereii and I worked out and he patched in the A19 core. It was when I added new graphics for the opening help screens. Those .png files dont unload, but he edited the code to force unloading." He stated this error was reported to TFP.
 
 With the help of Sphereii, I've gotten a DMT / Harmony fix to this error. Check the Notes.txt file.
 
 Here is my log for these three errors (yellow & red): https://pastebin.com/tnY4w4uZ
 
 
 
 CRAFTING QUESTS OBJECTIVE ISSUE
 ==============
 
 I noticed when doing "craft" quests for any classes, no matter which workbench a player has to craft at, you must be in the crafting screen, as it's being built at the workbench (being able to see the timer on the workbench) for it to work.
 
 In other words, you can't simply start crafting something (with a timer of say 1-2 minutes), leave the workbench/station you have started the craft at and do other things. Instead, you have to literally be in the crafting screen, as it's being built at the workbench for the quest to trigger as finished.
 
 This is problematic, as say a player takes hours to collect "electronic components" from cars, only for it not to work since they didn't sit in the crafting menu. Not to mention, it takes a very long time for crafting to even work at times. Also, if you were in multiplayer and only had one station/workbench you needed to craft at, it would prevent other players from crafting.
 
 I have confirmed with multiple people that this is unfortunately simply how the game works for crafting quests. Unfortunately there are only a few quests associated with crafting:
 
	Quests:
	
	quest_BasicSurvival1
	quest_BasicSurvival2
	quest_BasicSurvival3
	quest_BasicSurvival5
	quest_BasicSurvival6
	quest_BasicSurvival7
	quest_BasicSurvival8
	
 The other unfornate thing is that the only mention to the crafting quests is in the beginning of the quests.xml vanilla file. However, the following line could be a genuine fix, but it would come with it's caveats.
 
	<objective type="FetchKeep" id="resourceRockSmall" value="50" phase="1"/>
	
 For the Technician Class for example, you could change this code:
 
	<quest id="0wotwQuestClass_TechnicianPt1">
		<property name="name_key" value="0wotwQuestClass_TechnicianPt1_name"/>
		<property name="subtitle_key" value="0wotwQuestClass_TechnicianPt1_subtitle"/>
		<property name="description_key" value="0wotwQuestClass_TechnicianPt1_desc"/>
		<property name="offer_key" value="0wotwQuestClass_TechnicianPt1_offer"/>
		<property name="icon" value="ui_game_symbol_classTechnician"/>
		<property name="repeatable" value="true"/>
		<property name="category_key" value="Quest"/>
		<property name="difficulty" value="easy"/>
		<property name="shareable" value="false"/>
		
			<objective type="Craft" id="meleeToolWireTool" value="1" phase="1"/>
			<objective type="Craft" id="generatorbank" value="1" phase="2"/>
			<objective type="Craft" id="batterybank" value="1" phase="3"/>
			<objective type="Craft" id="electricwirerelay" value="5" phase="4"/>

		<reward type="Exp" value="10000"/>
		<reward type="Quest" id="0wotwQuestClass_TechnicianPt2"/>
	</quest>
	
	- to -
	
	<quest id="0wotwQuestClass_TechnicianPt1">
		<property name="name_key" value="0wotwQuestClass_TechnicianPt1_name"/>
		<property name="subtitle_key" value="0wotwQuestClass_TechnicianPt1_subtitle"/>
		<property name="description_key" value="0wotwQuestClass_TechnicianPt1_desc"/>
		<property name="offer_key" value="0wotwQuestClass_TechnicianPt1_offer"/>
		<property name="icon" value="ui_game_symbol_classTechnician"/>
		<property name="repeatable" value="true"/>
		<property name="category_key" value="Quest"/>
		<property name="difficulty" value="easy"/>
		<property name="shareable" value="false"/>
		
			<objective type="FetchKeep" id="meleeToolWireTool" value="1" phase="1"/>
			<objective type="FetchKeep" id="generatorbank" value="1" phase="2"/>
			<objective type="FetchKeep" id="batterybank" value="1" phase="3"/>
			<objective type="FetchKeep" id="electricwirerelay" value="5" phase="4"/>

		<reward type="Exp" value="10000"/>
		<reward type="Quest" id="0wotwQuestClass_TechnicianPt2"/>
	</quest>
	
 What would make this problematic, is that it would create a few caveats. For instance, if you happened to find a wire tool, generator bank, battery bank or a wire relay out and about, by buying it from a shop or somone else crafts it and then you pick it up, suddenly you don't have to go through the trouble of getting to certain perks, using points and/or finding/utilizing the resources to make the items in the first place.
 
 Interestingly enough, the following list (provided by Saminal) seems to be the only available objectives to utilize in quests.xml:
 
	FetchKeep
	Craft
	BlockPlace
	Wear
	BlockUpgrade
	Goto
	InteractWithNPC
	RandomGoto
	RallyPoint
	ZombieKill
	AnimalKill
	TreasureChest
	RandomPOIGoto
	FetchFromContainer
	POIStayWithin
	ReturnToNPC
	ClearSleepers
	FetchFromTreasure
	ClosestPOIGoto
	and yes - that is used with objectives:

	 <requirement type="Group" id="requirementGroupAxe" value="OR" phase="3"> means
	* use these requirements (following)
	* use the localization key requirementGroupAxe to describe the requirement
	* use the logic 'OR' for the multiple requirements - only one needs to be true
	* apply this requirement to the objective(s) labelled as phase 3
	
 Therefore, outside of using the objective "FetchKeep," there really isn't any other way to fix what I view as a bug (despite being a vanilla limitation).
 
 The other potential fix is using DMT and C#. If you were to implement the fix Sphereii came up with, then it would be worth looking into to make class quests that much more immersive.
 
 ================================
 
 
 
 MAPLE TREES BLOCK TEXTURES & GRASS BLOCK ISSUE
 ==============
 
 When reorganizing multiple items, blocks and item modifications in the creative menu and adding missing icons, I noticed that a few "maple trees" have absolutely terrible textures. I learned that they were originally in the vanilla files and it seems that you added them back into the game via WotW.

 Not all trees added by WotW look bad, However, the treeMaple17m, treeMaple16m, treeMaple15m, treeMaple13m and treeMaple6m all have warped textures. If you created new textures for these trees, they didn't go through.
 
 I also noticed that the "treePlainsGrassDiagonal" block is invisible in-game. While I corrected the missing icon, I do not have access to your original Unity files and I am unaware of which version of Unity you used, so therefore I cannot attempt to fix this item.

 All these trees have been hidden from the Creative Menu via code with icons provided to them (mirroring vanilla trees and similar heights), but they can still be viewed via the dev mode of the creative menu. However, I did completely remove the "treePlainsGrassDiagonal" block from the creative menu and dev mode.
 
 
 
 ================================
 
 
 
 SEDAN CARS NOT LOADING ONTO GROUND PROPERLY
 ==============
 
 I noticed a visual glitch with many Sedan models. I've noticed a yellow line error at the beginning of the game that mentioned something about a car being crushed and it could likely refer to issues like this.

 I believe that the reason Sedan Cars are going into the ground could have something to do do with the collider, voxel space, etc. etc on the models in Unity. Without any fixes, these cars actually loads into the pavement slightly instead of on top of the pavement.
 
 
 
 ================================